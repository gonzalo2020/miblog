import { DatosetiquetasService } from './../servicios/datosetiquetas.service';
import { DatosblogService } from './../servicios/datosblog.service';
import { Etiqueta } from './../modelos/etiqueta';
import { Blog } from './../modelos/blog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  public miNumero: number = 5;
  segundoNum: number = 2;
  tercerNum: number = 3;
  mensaje: string = 'hola' + "dobles" + `literales`;
  mostrar: boolean = false;
  verdad: boolean = false;
  arreglo: string[] = ["mi", "nuevo", "arreglo"];
  contenedor1: Blog[];
  contenedortitu: Blog[];
  contenedor2: Blog[];
  personas: Blog[];
  contenedoretiqueta:Etiqueta[];

  constructor(private servicio: DatosblogService,
    private servicioetiqueta:DatosetiquetasService) {
    this.servicio.getNumeroblogs(3).subscribe(informacion=>{
      this.contenedor1 = informacion;
    })

    this.servicio.getNumeroblogs(2).subscribe(informacion=>{
      this.personas = informacion;
    })

    this.servicio.getNumeroblogs(4).subscribe(informacion=>{
      this.contenedortitu = informacion;
    })

    this.servicio.getNumeroblogs(5).subscribe(informacion=>{
      this.contenedor2 = informacion;
    })

    this.servicioetiqueta.getNumeroetiqueta(2).subscribe(informacion=>{
      this.contenedoretiqueta = informacion;
    })

  }

  ngOnInit(): void {

  }
  public sumador(edad: number): number {
    return edad + 1;
  }

  public cambiador() {
    this.mostrar = !this.mostrar
  }

}
