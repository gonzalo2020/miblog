
import { Router } from '@angular/router';
import { Blog } from './../modelos/blog';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { DatosblogService } from '../servicios/datosblog.service';

@Component({
  selector: 'app-listablogs',
  templateUrl: './listablogs.component.html',
  styleUrls: ['./listablogs.component.scss']
})


export class ListablogsComponent {
  displayedColumns: string[] = ['id', 'titulo', 'fecha', 'contenido', 'foto','etiquetas','acciones'];
  dataSource = new MatTableDataSource();
  misblogs:Blog[];
  constructor(private servicio:DatosblogService,private ruta:Router) {
    this.servicio.getBlogs().subscribe(base=>{
      this.misblogs=base
      this.dataSource.data=this.misblogs
      // console.log(this.misblogs);
    })
    
   }
  
   editar(imprimir:Blog){
    this.ruta.navigateByUrl("/editarblog/"+imprimir.id)
   }
   borrar(imprimir:Blog){
    this.servicio.borrar(imprimir)
  }

}
