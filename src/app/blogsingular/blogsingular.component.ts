import { Blog } from './../modelos/blog';
import { DatosblogService } from './../servicios/datosblog.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-blogsingular',
  templateUrl: './blogsingular.component.html',
  styleUrls: ['./blogsingular.component.scss']
})
export class BlogsingularComponent implements OnInit {
  direccion:string;
  miblog:Blog;

  constructor(
    private serviciorutas:ActivatedRoute,
    private servicioblog:DatosblogService
  ) { 
    this.direccion= this.serviciorutas.snapshot.params.codigo
    this.servicioblog.getUnBlog(this.direccion).subscribe(datos=>{
      this.miblog=datos;
    })
  }

  ngOnInit(): void {
  }

}
