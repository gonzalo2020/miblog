import { Acercade } from './../modelos/acercade';
import { AcercadeService } from './../servicios/acercade.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acercade',
  templateUrl: './acercade.component.html',
  styleUrls: ['./acercade.component.scss']
})
export class AcercadeComponent implements OnInit {
  
  acercade: Acercade[];

  constructor(private servicio: AcercadeService) {
    this.servicio.getAcercade().subscribe(informacion=>{
      this.acercade = informacion;
      // console.log(this.acercade);
    })
   }

  ngOnInit(): void {
  }

}
