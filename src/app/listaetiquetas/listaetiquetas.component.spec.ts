import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaetiquetasComponent } from './listaetiquetas.component';

describe('ListaetiquetasComponent', () => {
  let component: ListaetiquetasComponent;
  let fixture: ComponentFixture<ListaetiquetasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaetiquetasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaetiquetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
