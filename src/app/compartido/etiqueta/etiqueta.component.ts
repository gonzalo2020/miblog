import { Etiqueta } from './../../modelos/etiqueta';
import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'mietiqueta',
  templateUrl: './etiqueta.component.html',
  styleUrls: ['./etiqueta.component.scss']
})
export class EtiquetaComponent implements OnInit {
  @Input("color") micolor:string;
  @Input("mensaje") texto:string;
  @Input("objeto") etiqueta:Etiqueta;
  constructor() { }

  ngOnInit(): void {
  }

}
