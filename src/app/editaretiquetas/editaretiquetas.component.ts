import { ActivatedRoute, Router } from '@angular/router';
import { DatosetiquetasService } from './../servicios/datosetiquetas.service';
import { Etiqueta } from './../modelos/etiqueta';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-editaretiquetas',
  templateUrl: './editaretiquetas.component.html',
  styleUrls: ['./editaretiquetas.component.scss']
})
export class EditaretiquetasComponent implements OnInit {
  direccion:string;
  minuevaetiqueta:Etiqueta;
  constructor(
    private serviciorutas:ActivatedRoute,
    private servicioetiqueta:DatosetiquetasService,
    private rutas:Router
  ) { 
    this.direccion= this.serviciorutas.snapshot.params.id
    this.servicioetiqueta.getUnaEtiqueta(this.direccion).subscribe(datos=>{
    this.minuevaetiqueta=datos;
    console.log(this.minuevaetiqueta)
    })
  }

  ngOnInit(): void {
  }
  imprimir(dato:Etiqueta){
    //  console.log(dato)
     this.servicioetiqueta.editarEtiqueta(dato)
     this.rutas.navigateByUrl("/listaretiquetas")
  
   }
}
