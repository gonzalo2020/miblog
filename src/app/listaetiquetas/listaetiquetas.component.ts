import { DatosetiquetasService } from './../servicios/datosetiquetas.service';
import { Etiqueta } from './../modelos/etiqueta';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listaetiquetas',
  templateUrl: './listaetiquetas.component.html',
  styleUrls: ['./listaetiquetas.component.scss']
})
export class ListaetiquetasComponent  {
  displayedColumns: string[] = [ 'id','titulo', 'color','acciones'];
  dataSource = new MatTableDataSource();
  misEtiquetas:Etiqueta[];

  constructor(private servicio:DatosetiquetasService,
    private ruta:Router) 
  {
      this.servicio.getEtiquetas().subscribe(base=>{
        this.misEtiquetas=base
        this.dataSource.data=this.misEtiquetas
        // console.log(this.misblogs);
      })
     }

editar(imprimir:Etiqueta){
      this.ruta.navigateByUrl("/editaretiquetas/"+imprimir.id)
      //console.log(imprimir);
}
 borrar(imprimir:Etiqueta){
  this.servicio.borrarEtiqueta(imprimir)
}

}
