import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaretiquetasComponent } from './editaretiquetas.component';

describe('EditaretiquetasComponent', () => {
  let component: EditaretiquetasComponent;
  let fixture: ComponentFixture<EditaretiquetasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditaretiquetasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaretiquetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
