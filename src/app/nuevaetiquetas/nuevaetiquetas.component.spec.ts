import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaetiquetasComponent } from './nuevaetiquetas.component';

describe('NuevaetiquetasComponent', () => {
  let component: NuevaetiquetasComponent;
  let fixture: ComponentFixture<NuevaetiquetasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevaetiquetasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaetiquetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
