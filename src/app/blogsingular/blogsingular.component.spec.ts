import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogsingularComponent } from './blogsingular.component';

describe('BlogsingularComponent', () => {
  let component: BlogsingularComponent;
  let fixture: ComponentFixture<BlogsingularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogsingularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogsingularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
