import { EditaretiquetasComponent } from './editaretiquetas/editaretiquetas.component';
import { EtiquetaComponent } from './compartido/etiqueta/etiqueta.component';
import { ListaetiquetasComponent } from './listaetiquetas/listaetiquetas.component';
import { EditarblogComponent } from './editarblog/editarblog.component';
import { ListablogsComponent } from './listablogs/listablogs.component';
import { NuevoblogComponent } from './nuevoblog/nuevoblog.component';
import { BlogsingularComponent } from './blogsingular/blogsingular.component';
import { AcercadeComponent } from './acercade/acercade.component';
import { InicioComponent } from './inicio/inicio.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevaetiquetasComponent } from './nuevaetiquetas/nuevaetiquetas.component';

const routes: Routes = [
{
  path:"",
  redirectTo:"inicio",
  pathMatch:"full"
},
  {
  path:"inicio",
  component:InicioComponent
},
{
  path:"acerca",
  component:AcercadeComponent
},
{
  path:"entrada/:codigo",
  component:BlogsingularComponent
},
{
  path:"nuevoblog",
  component:NuevoblogComponent
},
{
  path:"listablogs",
  component:ListablogsComponent
},
{
  path:"editarblog/:id",
  component:EditarblogComponent
},
{
  path:"listaretiquetas",
  component:ListaetiquetasComponent
},
{
  path:"nuevaetiqueta",
  component:NuevaetiquetasComponent
},
{
  path:"editaretiquetas/:id",
  component:EditaretiquetasComponent
},
{
  path:"**",
  redirectTo:"inicio"
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
