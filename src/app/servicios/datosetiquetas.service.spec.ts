import { TestBed } from '@angular/core/testing';

import { DatosetiquetasService } from './datosetiquetas.service';

describe('DatosetiquetasService', () => {
  let service: DatosetiquetasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatosetiquetasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
