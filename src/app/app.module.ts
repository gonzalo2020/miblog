import { CompartidoModule } from './compartido/compartido.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlexLayoutModule } from '@angular/flex-layout'; 
import { EtiquetaComponent } from './compartido/etiqueta/etiqueta.component';
import { AcercadeComponent } from './acercade/acercade.component';
import { BlogsingularComponent } from './blogsingular/blogsingular.component'; 

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { NuevoblogComponent } from './nuevoblog/nuevoblog.component';
import { ListablogsComponent } from './listablogs/listablogs.component';
import { EditarblogComponent } from './editarblog/editarblog.component';
import { ListaetiquetasComponent } from './listaetiquetas/listaetiquetas.component';
import { NuevaetiquetasComponent } from './nuevaetiquetas/nuevaetiquetas.component';
import { EditaretiquetasComponent } from './editaretiquetas/editaretiquetas.component';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    EtiquetaComponent,
    AcercadeComponent,
    BlogsingularComponent,
    NuevoblogComponent,
    ListablogsComponent,
    EditarblogComponent,
    ListaetiquetasComponent,
    NuevaetiquetasComponent,
    EditaretiquetasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    CompartidoModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
