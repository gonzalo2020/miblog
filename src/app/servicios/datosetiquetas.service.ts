import { Etiqueta } from './../modelos/etiqueta';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, pipe } from 'rxjs';
import { map } from "rxjs/operators";
import {MatSnackBar} from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class DatosetiquetasService {
  private collection: AngularFirestoreCollection<Etiqueta>;
  private listaEtiqueta: Observable<Etiqueta[]>;
  private documentoEtiqueta: AngularFirestoreDocument<Etiqueta>;

  constructor(private base: AngularFirestore, private notificaciones:MatSnackBar) {
    this.collection = this.base.collection<Etiqueta>("etiquetas");
   }
   getEtiquetas() {
    this.listaEtiqueta = this.collection.snapshotChanges().pipe(
      map(retorno => retorno.map(datos => {
        const documento = datos.payload.doc.data() as Etiqueta
        const id = datos.payload.doc.id;
        return {
          id,
          ... documento
        }
      }))
    )
    return this.listaEtiqueta;
  }
  getUnaEtiqueta(codigo: string) {
    return this.collection.doc(codigo).snapshotChanges().pipe(
      map(retorno=>{
        const documento = retorno.payload.data() as Etiqueta
        const id = retorno.payload.id;
        return {
          id,
          ...documento
        }
      })
    )
    
  }
  getNumeroetiqueta(cantidad: number) {
    this.listaEtiqueta=this.base.collection<Etiqueta>("etiqueta",qr=>qr.limit(cantidad)).snapshotChanges().pipe(
      map(retorno=>retorno.map(datos=>{
        const documento = datos.payload.doc.data() as Etiqueta
        const id = datos.payload.doc.id;
        return {
          id,
          ... documento
        }
      }))
    )
    return this.listaEtiqueta;
  }

  crearEtiqueta(guardar:Etiqueta){
    this.collection.add(guardar)
    this.notificaciones.open("Guardado en la base","OK..!",{duration:3000})
  }

  borrarEtiqueta(imprimir:Etiqueta){
    this.documentoEtiqueta = this.collection.doc(imprimir.id)
    this.documentoEtiqueta.delete()
    this.notificaciones.open("Dato eliminado","OK..!",{duration:3000})
  }
  
  editarEtiqueta(imprimir:Etiqueta){
    this.documentoEtiqueta = this.collection.doc(imprimir.id)
    this.documentoEtiqueta.update(imprimir)
   
  }

}
