import { Blog } from './../modelos/blog';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, pipe } from 'rxjs';
import { map } from "rxjs/operators";
import {MatSnackBar} from '@angular/material/snack-bar';



@Injectable({
  providedIn: 'root'
})
export class DatosblogService {
  private collection: AngularFirestoreCollection<Blog>;
  private listaBlogs: Observable<Blog[]>;
  private documentoBlog: AngularFirestoreDocument<Blog>;

  constructor(private base: AngularFirestore, private notificaciones:MatSnackBar) {
    this.collection = this.base.collection<Blog>("blog");
  }
  getBlogs() {
    this.listaBlogs = this.collection.snapshotChanges().pipe(
      map(retorno => retorno.map(datos => {
        const documento = datos.payload.doc.data() as Blog
        const id = datos.payload.doc.id;
        return {
          id,
          ... documento
        }
      }))
    )
    return this.listaBlogs;
  }
  getNumeroblogs(cantidad: number) {
    this.listaBlogs=this.base.collection<Blog>("blog",qr=>qr.limit(cantidad)).snapshotChanges().pipe(
      map(retorno=>retorno.map(datos=>{
        const documento = datos.payload.doc.data() as Blog
        const id = datos.payload.doc.id;
        return {
          id,
          ... documento
        }
      }))
    )
    return this.listaBlogs;
  }
  getUnBlog(codigo: string) {
    return this.collection.doc(codigo).snapshotChanges().pipe(
      map(retorno=>{
        const documento = retorno.payload.data() as Blog
        const id = retorno.payload.id;
        return {
          id,
          ...documento
        }
      })
    )
    
  }
  crearBlog(guardar:Blog){
    this.collection.add(guardar)
    this.notificaciones.open("Guardado en la base","OK..!",{duration:3000})
  }

  borrar(imprimir:Blog){
    this.documentoBlog = this.collection.doc(imprimir.id)
    this.documentoBlog.delete()
    this.notificaciones.open("Dato eliminado","OK..!",{duration:3000})
  }
  
  editarUnblog(imprimir:Blog){
    this.documentoBlog = this.collection.doc(imprimir.id)
    this.documentoBlog.update(imprimir)
   
  }

}
