import { DatosetiquetasService } from './../servicios/datosetiquetas.service';
import { Etiqueta } from './../modelos/etiqueta';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosblogService } from './../servicios/datosblog.service';
import { Blog } from './../modelos/blog';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-editarblog',
  templateUrl: './editarblog.component.html',
  styleUrls: ['./editarblog.component.scss']
})
export class EditarblogComponent implements OnInit {
  direccion:string;
  miblog:Blog;
  misEtiquetas:Etiqueta[];

  constructor(
    private serviciorutas:ActivatedRoute,
    private servicioblog:DatosblogService,
    private rutas:Router,
    private servicio:DatosetiquetasService
  ) {
    this.direccion= this.serviciorutas.snapshot.params.id
    this.servicioblog.getUnBlog(this.direccion).subscribe(datos=>{
    this.miblog=datos;
    console.log(this.miblog)
    })

    this.servicio.getEtiquetas().subscribe(informacion=>{
      this.misEtiquetas = informacion;
      // console.log(this.acercade);
    })
   }

  ngOnInit(): void {
  }
 imprimir(dato:Blog){
  //  console.log(dato)
   this.servicioblog.editarUnblog(dato)
   this.rutas.navigateByUrl("/listablogs")

 }
 verificador(verificar1:Blog,verificar2:Blog){
  //muestra.indexOf(verificar);
  if(verificar1.id === verificar2.id){
    return true;
  }
  else{
    return false;
  }
 }
 
}
