import { DatosetiquetasService } from './../servicios/datosetiquetas.service';
import { Etiqueta } from './../modelos/etiqueta';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-nuevaetiquetas',
  templateUrl: './nuevaetiquetas.component.html',
  styleUrls: ['./nuevaetiquetas.component.scss']
})
export class NuevaetiquetasComponent implements OnInit {
  minuevaetiqueta:Etiqueta={
    mensaje:"",
    color:"",
  }
  constructor(private base:DatosetiquetasService) { }

  ngOnInit(): void {
  }
  imprimir(formulario:NgForm){
    // console.log(this.minuevoblog)
    this.base.crearEtiqueta(this.minuevaetiqueta);
    formulario.resetForm();
  }
}
