import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarblogComponent } from './editarblog.component';

describe('EditarblogComponent', () => {
  let component: EditarblogComponent;
  let fixture: ComponentFixture<EditarblogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarblogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
