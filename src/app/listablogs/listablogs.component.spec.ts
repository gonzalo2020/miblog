import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListablogsComponent } from './listablogs.component';

describe('ListablogsComponent', () => {
  let component: ListablogsComponent;
  let fixture: ComponentFixture<ListablogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListablogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListablogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
