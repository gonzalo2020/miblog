import { Etiqueta } from './../modelos/etiqueta';
import { Blog } from './../modelos/blog';
import { Component, OnInit } from '@angular/core';
import { DatosblogService } from '../servicios/datosblog.service';
import { NgForm } from '@angular/forms';
import {FormControl} from '@angular/forms';
import { DatosetiquetasService } from './../servicios/datosetiquetas.service';


@Component({
  selector: 'app-nuevoblog', 
  templateUrl: './nuevoblog.component.html',
  styleUrls: ['./nuevoblog.component.scss']
 
})
export class NuevoblogComponent implements OnInit {

  minuevoblog:Blog={
    titulo:"",
    fecha:"",
    contenido:"",
    foto:"",
    etiquetas:[]
  }
  misEtiquetas:Etiqueta[];

  constructor(private base:DatosblogService,
    private servicio:DatosetiquetasService) {
      this.servicio.getEtiquetas().subscribe(informacion=>{
        this.misEtiquetas = informacion;
        // console.log(this.acercade);
      })
      
     }

  ngOnInit(): void {
  }

  imprimir(formulario:NgForm){
     //console.log(this.minuevoblog)
    this.base.crearBlog(this.minuevoblog);
    formulario.resetForm();


  }
}
