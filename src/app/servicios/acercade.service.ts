import { AcercadeComponent } from './../acercade/acercade.component';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, pipe } from 'rxjs';
import { map } from "rxjs/operators";
import { Acercade } from '../modelos/acercade';

@Injectable({
  providedIn: 'root'
})
export class AcercadeService {
  private collection: AngularFirestoreCollection<Acercade>;
  private documentoacercade:AngularFirestoreDocument<Acercade>;
  private listaacercade: Observable<Acercade[]>;


  constructor(private base: AngularFirestore) {
    this.collection = this.base.collection<Acercade>("acercade");
   }

   getAcercade() {
    this.listaacercade = this.collection.snapshotChanges().pipe(
      map(retorno => retorno.map(datos => {
        const documento = datos.payload.doc.data() as Acercade
        const id = datos.payload.doc.id;
        return {
          id,
          ...documento
        }
      }))
    )
    return this.listaacercade;
  }

}
