import { Etiqueta } from './etiqueta';
export interface Blog {
    titulo:string,
    fecha:string,
    foto?:string,
    etiquetas?:Etiqueta[],
    id?:string,
    contenido:string
}
